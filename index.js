const { exec } = require("child_process");

var glob = require("glob")

// let exception = ['admin'];
let exception = [];

glob("../**/*compose.yml", function (er, files) {
  files.forEach(file => {
    if (file.indexOf('node_modules') == -1) { // все докеры 
      exception.forEach(exex => {
        if (file.indexOf(exex) == -1) { // все докеры  после исключений
          exec(`cd ${file.split('docker-compose.yml')[0]} && docker-compose up`, (error, stdout, stderr) => {
            if (error) {
              console.log(`error: ${error.message}`);
              return;
            }
            if (stderr) {
              console.log(`stderr: ${stderr}`);
              return;
            }
          });
        }
      })
    }
  })
})